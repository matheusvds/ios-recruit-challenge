//
//  MovieCollectionViewCell.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 03/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleBackground: UIView!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!

}
