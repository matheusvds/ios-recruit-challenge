//
//  FavoritesTableViewCell.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 05/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import UIKit

class FavoritesTableViewCell: UITableViewCell {
    @IBOutlet weak var favoriteImage: UIImageView!
    
    @IBOutlet weak var favoriteName: UILabel!
    
    @IBOutlet weak var favoriteYear: UILabel!
    @IBOutlet weak var favoriteDescription: UILabel!
    
}
