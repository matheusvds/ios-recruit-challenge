//
//  Movie.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 03/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation
import UIKit

struct MovieList : Codable {
    let results: [Movie]
}

struct Movie : Codable {
    let id : Int
    let title: String
    let poster_path: String    
    let release_date: String
    let genre_ids: [Int]
    let overview: String
}
