//
//  TheMovieDB.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 03/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation

enum TheMovieDB {

    case genres
    case popularMovies(page: Int)
    case images(size: String, path: String)
    
    var baseURL: String {
        switch self {
        case .genres, .popularMovies:
            return K.APIServer.baseURL
        case .images:
            return K.APIServer.imgBaseURL
        }
    }
    
    var path: String {
        switch self {
        case .genres:
            return "/genre/movie/list?api_key=\(K.APIParameterKey.key)"
        case .popularMovies(let page):
            return "/movie/popular?api_key=\(K.APIParameterKey.key)&page=\(page)"
        case .images(let size, let path):
            return "/t/p/\(size)/\(path)"
        }
    }
    
    func asURLRequest() -> URLRequest {
        let urlString = "\(baseURL)\(path)"
        let url = URL(string: urlString)!
        let urlRequest = URLRequest(url: url)
        return urlRequest
    }

}
