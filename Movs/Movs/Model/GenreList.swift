//
//  GenreList.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 04/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation

struct GenreList: Codable {
    let genres: [Genre]
}

struct Genre: Codable {
    let id: Int
    let name: String
}
