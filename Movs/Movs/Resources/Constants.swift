//
//  Constants.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 03/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation

struct K {
    struct APIServer {
        static let baseURL = "https://api.themoviedb.org/3"
        static let imgBaseURL = "https://image.tmdb.org"
    }
    
    struct APIParameterKey {
        static let key = "780517cdd65386623d21c096f42be2ba"
    }
}

enum RequestError : Error {
    case RequestUnkown
    case JSONError
    case URLFormation
}
