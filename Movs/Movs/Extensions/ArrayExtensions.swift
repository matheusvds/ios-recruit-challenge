//
//  ArrayExtensions.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 07/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}


