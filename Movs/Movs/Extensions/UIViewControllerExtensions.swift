//
//  UIViewControllerExtensions.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 03/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor(red:0.03, green:0.03, blue:0.03, alpha:0.5)
        let indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        indicator.startAnimating()
        indicator.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(indicator)
            onView.addSubview(spinnerView)
        }
        return spinnerView
    }
}

