//
//  UICollectionViewExtensions.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 06/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView {
    
    func setEmptyMessage() {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width*0.7, height: self.bounds.size.height))
        messageLabel.text = "Sorry! We could not find any movies with that name. :("
        messageLabel.textColor = UIColor(red:1, green:1, blue:1, alpha:0.7)
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 30)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
    }
    
    func restore() {
        self.backgroundView = nil
    }
}
