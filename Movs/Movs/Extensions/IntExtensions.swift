//
//  IntegerExtensions.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 05/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation

extension Int {
    var boolValue: Bool { return self != 0 }
}
