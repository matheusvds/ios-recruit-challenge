//
//  StringExtensions.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 04/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation

extension String {
    func formatDate() -> String {
        let endingIndex = self.index(self.startIndex, offsetBy: 4)
        return String(self[..<endingIndex])
    }
    
    func removeLast(characters: Int) -> String {
        let endingIndex = self.index(self.endIndex, offsetBy: -characters)
        return String(self[..<endingIndex])
    }
}
