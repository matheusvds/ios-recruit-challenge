//
//  FilterViewController.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 07/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation
//
//  DetailViewController.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 04/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import UIKit

protocol FilterDelegate {
    func filterBy(year: String, andGenre genre: Int)
}

class FilterViewController: UIViewController {
    
     var delegate:FilterDelegate?
    
    @IBOutlet weak var genreField: UITextField!
    @IBOutlet weak var yearField: UITextField!
    var genres: [Genre]?
    var years: [String]?
    let pickerView = UIPickerView()

    override func viewWillAppear(_ animated: Bool) {
        retrieveGenres()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pickerView.delegate = self
        genreField.inputView = self.pickerView
        yearField.inputView = self.pickerView
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: - IBActions Methods
    @IBAction func applyButtonPressed(_ sender: Any) {
        tryPassingDataBack()
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
  
}

//MARK: - PickerView DataSoruce
extension FilterViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let genres = self.genres, let years = self.years {
            return (component == 1 ? genres.count : years.count)
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let genres = self.genres, let years = self.years {
            return (component == 1 ? genres[row].name : years[row])
        }
        return ""
    }
}
//MARK: - PickerView Delegate
extension FilterViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        setFieldsFor(component, andRow: row)
    }
}

//MARK: - Helper Methods
extension FilterViewController {
    
    
    //MARK: - PickerView
    func setFieldsFor(_ component: Int, andRow row: Int) {
        guard let genres = self.genres, let years = self.years else { return }
            if component == 1 {
                self.genreField.text = genres[row].name
            }
            else {
                self.yearField.text = years[row]
            }
    }
    
    // MARK: - ViewController
    func retrieveGenres () {
        let spinner = UIViewController.displaySpinner(onView: self.view)
        NetworkController.fetchGenresResults { (fetchedGenres) in
            DispatchQueue.main.async {
                spinner.removeFromSuperview()
            }
//            let genreList = fetchedGenres.flatMap({ (genre) -> String? in
//                return genre.name
//            })
//            self.genres = Array(Set(genreList))
            self.genres = fetchedGenres
        }
    }
    
    func tryPassingDataBack() {
        if self.genreField.text != "" && self.yearField.text != "" {
            guard let genres = genres else { return }
            let genre = genres.filter({$0.name.elementsEqual(self.genreField.text!)})[0]
            delegate?.filterBy(year: self.genreField.text!, andGenre: genre.id)
            presentingViewController?.dismiss(animated: true, completion: nil)
        }
        else {
            showAlert()
        }
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "No selection", message: "Please, make sure you selected the right options", preferredStyle: .alert)
        let action = UIAlertAction(title: "Sorry! I'll try again", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
    
    
}
