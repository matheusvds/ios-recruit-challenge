//
//  FavoritesViewController.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 03/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {
    
    var deleteFavoriteIndexPath: IndexPath? = nil
    
    @IBOutlet weak var doneFilterButton: UIBarButtonItem! 
    @IBOutlet weak var empyStateImageView: UIImageView!
    @IBOutlet weak var favoritesTableview: UITableView!
    var rowHeight: CGFloat = 190.0
    var filteredFavorites = [Movie]()
    var filterIsActive: Bool = false {
        didSet {
            self.doneFilterButton.isEnabled = filterIsActive
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showEmptyState()
        self.doneFilterButton.isEnabled = filterIsActive
        favoritesTableview.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - IBOutlet Methods
    @IBAction func doneFilterButtonPressed(_ sender: Any) {
        self.filterIsActive = false
        self.favoritesTableview.reloadData()
    }

}

//MARK: - UITableViewDataSource
extension FavoritesViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return favorites.count
        return self.favorites(forFilterState: self.filterIsActive).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoritesCell", for: indexPath) as! FavoritesTableViewCell
        let favoriteMovie = self.favorites(forFilterState: self.filterIsActive)[indexPath.row];
        set(cell, for: favoriteMovie)
        return cell
    }
}

//MARK: - UITableViewDelegate
extension FavoritesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return getRowHeightFordeviceOrientation()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteFavoriteIndexPath = indexPath
            let favoriteToRemove = favorites[indexPath.row]
            confirmDelete(favorite: favoriteToRemove)
        }
    }
}

// MARK: - Filter Delegate
extension FavoritesViewController: FilterDelegate {
    
    func filterBy(year: String, andGenre genre: Int) {
        self.filteredFavorites = self.favorites.filter({
            return ($0.genre_ids.contains(genre) || $0.release_date.formatDate().elementsEqual(year))
        })
        print(self.favorites.count)
        print(self.filteredFavorites.count)
        self.filterIsActive = true
        self.favoritesTableview.reloadData()
    }
}


// MARK: - Navigation
extension FavoritesViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        sendDataForViewControllerWith(segue, with: sender)
    }
}

//MARK: - Helper Methods
extension FavoritesViewController {
    
    //MARK: - Computed Properties
    var movies: [Movie] {
        get {
            let navigationController = self.tabBarController!.viewControllers![0] as! UINavigationController
            let homeVC = navigationController.viewControllers.first! as! HomeViewController
            return homeVC.movies
        }
    }
    
    var favorites: [Movie] {
        return movies.filter({ PersistanceController.isFavorite($0) })
    }
    
    //MARK: - ViewController
    
    //MARK - Navigation
    func getMovieDetailsFrom(sender: Any?, completion: @escaping ((Movie)-> Void)) {
        if let cell = sender as? FavoritesTableViewCell {
            guard let indexPath = self.favoritesTableview.indexPath(for: cell) else {return}
            let movie = self.favorites(forFilterState: self.filterIsActive)[indexPath.row]
            completion(movie)
        }
    }
    
    func sendDataForViewControllerWith(_ segue: UIStoryboardSegue, with sender: Any?) {
        if segue.destination is DetailViewController {
            getMovieDetailsFrom(sender: sender, completion: { (movie) in
                self.send(movie, withSegue: segue)
            })
        }
        if segue.destination is FilterViewController {
            self.sendFilterInfoWith(segue: segue)
        }
    }
    
    func send(_ movie: Movie, withSegue segue: UIStoryboardSegue) {
        if let vc = segue.destination as? DetailViewController {
            vc.movie = movie
        }
    }
    
    func sendFilterInfoWith(segue: UIStoryboardSegue) {
        if let vc = segue.destination as? FilterViewController {
            let yearList = movies.flatMap({ (movie) -> String? in
                return movie.release_date.formatDate()
            })
            vc.delegate = self
            vc.years = Array(Set(yearList))
        }
    }
    
    //MARK: - TableView
    
    func favorites(forFilterState state: Bool) -> [Movie] {
        return state ? self.filteredFavorites : self.favorites

    }
    
    func showEmptyState() {
        if self.favorites(forFilterState: self.filterIsActive).isEmpty {
            self.favoritesTableview.isHidden = true
            self.empyStateImageView.isHidden = false
        }
        else {
            self.favoritesTableview.isHidden = false
            self.empyStateImageView.isHidden = true
        }
    }
    
    func set(_ cell: FavoritesTableViewCell, for favoriteMovie: Movie) {
        
        cell.favoriteName.text = favoriteMovie.title
        cell.favoriteDescription.text = favoriteMovie.overview
        cell.favoriteYear.text = favoriteMovie.release_date.formatDate()
        NetworkController.fetchImage(path: favoriteMovie.poster_path) { (image) in
            DispatchQueue.main.async {
                cell.favoriteImage.image = image
            }
        }
    }
    
    
    func getRowHeightFordeviceOrientation() -> CGFloat {
        let currentOrientation = UIDevice.current.orientation
        if currentOrientation == .landscapeLeft || currentOrientation == .landscapeRight {
            return rowHeight*2
        } else {
            return rowHeight
        }
    }
    
    func confirmDelete(favorite: Movie) {
        let alert = UIAlertController(title: "Delete Favorite", message: "You are removing \(favorite.title) from your favorites. Are you sure?", preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: handleDeleteFavorite)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: cancelDeleteFavorite)
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func handleDeleteFavorite(alertAction: UIAlertAction!) -> Void {
        if let indexPath = deleteFavoriteIndexPath {
            favoritesTableview.beginUpdates()
            
            let favoriteToRemove = favorites[indexPath.row]
            PersistanceController.toggleFavoriteFor(favoriteToRemove)
            favoritesTableview.deleteRows(at: [indexPath as IndexPath], with: .automatic)
            deleteFavoriteIndexPath = nil
            
            favoritesTableview.endUpdates()
            showEmptyState()
        }
    }
    
    func cancelDeleteFavorite(alertAction: UIAlertAction!) {
        deleteFavoriteIndexPath = nil
    }
}

