//
//  HomeViewController.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 03/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation
import UIKit

class HomeViewController : UIViewController {
    

    @IBOutlet weak var appIcon: UIBarButtonItem!
    var movies = [Movie]()
    var filteredMovies:[Movie] = []
    var isDownloadingData: Bool = false  {
        didSet(oldValue) {
            if (oldValue == false && isDownloadingData == true) {
                self.page += 1
                self.setMovies(inPage: self.page)
            }
        }
    }
    var searchActive: Bool = false
    var page: Int = 1
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var moviesCollection: UICollectionView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        moviesCollection.reloadData()
        self.definesPresentationContext = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addNavBarImage()
        configureSearchController()
        setMovies(inPage: page)
    }
}

// MARK: - SearchController

extension HomeViewController : UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating {

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        self.moviesCollection.reloadData()
        searchBar.endEditing(true)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterResults(from: searchController)
        setEmptyState()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        reloadCollectionView()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = true
        reloadCollectionView()
    }
}


// MARK: - CollectionView DataSource
extension HomeViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "activityIndicatorView", for: indexPath) as! footerCollectionReusableView
        startActivityIn(view: footerView, forState: self.isDownloadingData)
        return footerView
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieData(forSearchState: searchActive).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        let movie: Movie = movieData(forSearchState: searchActive)[indexPath.row]
        setCellProperties(cell, for: movie)
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        detectedEndingOf(scrollView)
    }
    
    func detectedEndingOf(_ scrollView: UIScrollView) {
        if ((scrollView.contentOffset.y + 49) >= scrollView.contentSize.height - scrollView.frame.size.height) {
            self.isDownloadingData = true
        }
    }

}

// MARK: - CollectionView Delegates
extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat = 30
        let collectionCellSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionCellSize/2, height: collectionCellSize*1.5/2)
    }
}

// MARK: - Navigation
extension HomeViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.destination is DetailViewController {
            getMovieDetailsFrom(sender: sender, completion: { (movie) in
                self.send(movie, withSegue: segue)
            })
        }
    }
}

// MARK: - Helper Methods
extension HomeViewController {

    
    //MARK: - ViewController
    func addNavBarImage() {
        let image = #imageLiteral(resourceName: "tabbaricon")
        appIcon.setBackgroundImage(image, for: .normal, barMetrics: .default)
    }
    
    
    func setCellProperties(_ cell : MovieCollectionViewCell, for movie: Movie) {
        cell.title.text = movie.title
        PersistanceHelper.showFavorite(movie: movie, in: cell.favoriteButton)
        cell.favoriteButton.addTarget(self, action: #selector(favoriteButton(sender:)), for: .touchUpInside)
        setImage(path: movie.poster_path, imgView: cell.background)
    }
    
    func setMovies(inPage page: Int) { //This function can only be called one time
        let spinner = loading(self.view)
        NetworkController.fetchMovieResults(onPage: page) { (movies) in
            self.remove(spinner)
            self.appendNewMovies(movies)
            self.isDownloadingData = false
            DispatchQueue.main.async {
                self.moviesCollection.reloadData()
            }
        }
    }
    
    func appendNewMovies(_ movies: [Movie]) {
        for movie in movies {
            self.movies.append(movie)
        }
    }
    
    func configureSearchController() {
        filteredMovies = movies
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        self.searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search for movies"
        searchController.searchBar.sizeToFit()
        
        searchController.searchBar.becomeFirstResponder()
        
        self.navigationItem.titleView = searchController.searchBar
    }
    
    func loading(_ view : UIView) -> UIView {
        return  UIViewController.displaySpinner(onView: view)
    }
    
    func remove(_ spinner: UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
    
    func setImage(path: String, imgView :UIImageView) {
        let spin = loading(imgView)
        NetworkController.fetchImage(path: path) { (image) in
            DispatchQueue.main.async {
                self.remove(spin)
                imgView.image = image
            }
        }
    }
    
    //MARK: - Navigation
    
    
    func getMovieDetailsFrom(sender: Any?, completion: @escaping ((Movie)-> Void)) {
        if let cell = sender as? MovieCollectionViewCell {
            guard let indexPath = self.moviesCollection.indexPath(for: cell) else {return}
            let movie = self.movies[indexPath.row]
            completion(movie)
        }
    }
    
    func send(_ movie: Movie, withSegue segue: UIStoryboardSegue) {
        if let vc = segue.destination as? DetailViewController {
            vc.movie = movie
        }
    }
    
    //MARK: - CollectionView
    func filterResults(from searchController: UISearchController) {
        if searchController.searchBar.text! == "" {
            filteredMovies = movies
        } else {
            filteredMovies = movies.filter { $0.title.lowercased().contains(searchController.searchBar.text!.lowercased()) }
            self.moviesCollection.reloadData()
        }
    }
    
    func setEmptyState() {
        if filteredMovies.isEmpty && searchActive {
            self.moviesCollection.setEmptyMessage()
        }
        else {
            self.moviesCollection.restore()
        }
    }
    
    
    func startActivityIn(view: footerCollectionReusableView, forState isLoading: Bool) {
        if (isLoading) {
            view.infiniteActivityIndicator.startAnimating()
        }
        else {
            view.infiniteActivityIndicator.stopAnimating()
        }
    }
    
    @objc func favoriteButton(sender: UIButton){
        guard let cell = sender.superview?.superview as? UICollectionViewCell else {return}
        if let indexPath = self.moviesCollection.indexPath(for: cell) {
            let movie = self.movies[indexPath.row]
            PersistanceHelper.toggleFavoriteButtonImage(movie: movie, in: sender)
        }
    }
    
    func reloadCollectionView() {
        if self.searchController.searchBar.text != "" {
            moviesCollection.reloadData()
        }
    }

    func movieData(forSearchState state: Bool) -> [Movie] {
        if state {
            return self.filteredMovies
        }
        else {
            return self.movies
        }
    }
}
