//
//  DetailViewController.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 04/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var movieDetails: UILabel!
    @IBOutlet weak var movieGenres: UILabel!
    @IBOutlet weak var movieYear: UILabel!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    var movie: Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMovie()
    }
    
    //MARK: -IBActions Methods
    @IBAction func favoriteButtonPressed(_ sender: Any) {
        guard let movie = self.movie else { return }
        PersistanceHelper.toggleFavoriteButtonImage(movie: movie, in: sender as! UIButton)
        
    }
    @IBAction func closeButtonPressed(_ sender: Any) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - Helper Methods
extension DetailViewController {
    
    //MARK - ViewController
    func setMovie(){
        guard let movie = self.movie else { return }
        NetworkController.fetchImage(path: movie.poster_path) { (image) in
            self.movieImage.image = image
        }
        self.movieName.text = movie.title
        self.movieYear.text = movie.release_date.formatDate()
        self.movieDetails.text = movie.overview
        self.movieDetails.layer.cornerRadius = 20
        self.movieDetails.clipsToBounds = true
        PersistanceHelper.showFavorite(movie: movie, in: self.favoriteButton)
        
        setGenres(genres: movie.genre_ids) { (genresString) in
            DispatchQueue.main.async {
                self.movieGenres.text = genresString
            }
        }
    }
    
    func setGenres(genres: [Int], completion:@escaping ((String)->Void)) {
        var genreString = ""
        NetworkController.fetchGenresResults { (fetchedGenres) in
            let equalGenres = fetchedGenres.filter({ (element) -> Bool in
                return genres.contains(where: {$0 == element.id})
            })
            for genre in equalGenres {
                genreString.append("\(genre.name), ")
            }
            completion(genreString.removeLast(characters: 2))
        }
    }
}
