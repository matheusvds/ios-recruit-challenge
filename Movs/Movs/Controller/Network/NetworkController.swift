//
//  NetworkController.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 03/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation
import UIKit

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

class NetworkController {
    
    private static func perform(requestType : TheMovieDB, completion: @escaping ((Data) -> Void)) {
        
        let request = TheMovieDB.asURLRequest(requestType)
        URLSession.shared.dataTask(with: request() ) { (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }
            guard let data = data else { return }
            completion(data)
        }.resume()
        
    }
 
    static func getDataForModel<T: Decodable>(_: T.Type, requestType: TheMovieDB, completion: ((Result<[T]>) -> Void)?) {
        perform(requestType: requestType) { (retrievedData) in
            do {
                let data = try [JSONDecoder().decode(T.self , from: retrievedData)]
                completion?(.success(data))
            } catch let jsonError {
                completion?(.failure(jsonError))
            }
        }
    }
    
    
    static func getImage(path: String, completion: ((Result<UIImage>) -> Void)?) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let cache = appDelegate.imageCache
        if let cachedImage = cache.object(forKey: path as NSString) as? UIImage {
            completion?(.success(cachedImage))
        }
        else {
            perform(requestType: .images(size: "w300", path: path)) { (data) in
                if let image = UIImage(data: data) {
                    cache.setObject(image, forKey: path as NSString)
                    completion?(.success(image))
                }
                else {
                    let placeholder = #imageLiteral(resourceName: "first")
                    completion?(.success(placeholder))
                }
            }
        }
    }
    
    
    static func fetchMovieResults(onPage page: Int,completion:@escaping (([Movie])-> Void )) {
        getDataForModel(MovieList.self, requestType: TheMovieDB.popularMovies(page: page)) { (results) in
            switch results {
            case .success(let movies):
                if let moviesArray = movies.first {
                    completion(moviesArray.results)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion([])
            }
        }
    }
    
    static func fetchGenresResults(completion:@escaping (([Genre])-> Void )) {
        getDataForModel(GenreList.self, requestType: TheMovieDB.genres) { (results) in
            switch results {
            case .success(let genresList):
                if let genresList = genresList.first {
                    completion(genresList.genres)
                }
            case .failure(let error):
                print(error.localizedDescription)
                completion([])
            }
        }
    }
    
    static func fetchImage(path: String, completion: @escaping ((UIImage) -> Void)) {
        getImage(path: path) { (results) in
            switch results {
            case .success(let image):
                completion(image)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
