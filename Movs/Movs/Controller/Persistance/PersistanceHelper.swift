//
//  PersistanceHelper.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 05/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation
import UIKit

class PersistanceHelper {
    
    //MARK: - Helper Methods
    
    static func showFavorite(movie: Movie,in favoriteButton: UIButton) {
        let isFavorite = PersistanceController.isFavorite(movie)
        if isFavorite {
            let filledHearthIcon = UIImage(named: "favorite_full_icon")
            favoriteButton.setBackgroundImage(filledHearthIcon, for: .normal)
        }
        else {
            let emptyHearthIcon = UIImage(named: "favorite_empty_icon")
            favoriteButton.setBackgroundImage(emptyHearthIcon, for: .normal)
        }
    }
    
    static func toggleFavoriteButtonImage(movie: Movie,in favoriteButton: UIButton) {
        PersistanceController.toggleFavoriteFor(movie)
        showFavorite(movie: movie, in: favoriteButton)
    }
}
