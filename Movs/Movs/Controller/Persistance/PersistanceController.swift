//
//  PersistanceController.swift
//  Movs
//
//  Created by Matheus Vasconcelos on 05/02/18.
//  Copyright © 2018 matheus. All rights reserved.
//

import Foundation

class PersistanceController {
    
    static func isFavorite(_ movie: Movie) -> Bool{
        let defaults = UserDefaults.standard
        let isFavorite = defaults.integer(forKey: String(movie.id))
        return isFavorite.boolValue
    }
    
    static func toggleFavoriteFor(_ movie: Movie){
        let defaults = UserDefaults.standard
        let isFavorite = self.isFavorite(movie)
        defaults.set(!isFavorite, forKey: String(movie.id))
    }

}

